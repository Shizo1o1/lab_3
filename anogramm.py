class anagram():
		
	def testAnagram(self):
	    alist1 = list(input())
	    alist2 = list(input())

	    alist1.sort()
	    alist2.sort()

	    pos = 0
	    matches = True

	    while pos < len(alist1) and matches:
	        if alist1[pos]==alist2[pos]:
	            pos = pos + 1
	        else:
	            matches = False

	    if matches == True:
	    	print("Являются анаграммами!")
	    else:
	    	print("Не являются анаграммами!")	

test = anagram()
test.testAnagram()
